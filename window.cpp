#include "window.h"

//setRule
void Window::setRule(int r)
{
    board->setRule(r);
    Menus[0]->hide();
    Menus[1]->show();
}
void Window::setRule1()
{
    setRule(1);
}
void Window::setRule2()
{
    setRule(2);
}
//setGameEnd
void Window::setGameEnd(int g)
{
    board->setGameEnd(g);
    Menus[1]->hide();
    Menus[2]->show();
}
void Window::setGameEnd0()
{
    setGameEnd(0);
}
void Window::setGameEnd1()
{
    setGameEnd(1);
}
void Window::setGameEnd2()
{
    setGameEnd(2);
}
void Window::backg()
{
    Menus[1]->hide();
    Menus[0]->show();
}
//setVariant
void Window::setVariant(int v)
{
    board->setVariant(v);
    Menus[2]->hide();
    board->show();
}
void Window::setVariant0()
{
    setVariant(0);
}
void Window::setVariant1()
{
    setVariant(1);
}
void Window::backv()
{
    Menus[2]->hide();
    Menus[1]->show();
}

void Window::chooseRule(){
    Menus[0]->show();
}


Window::Window() : QWidget()
{
    setFixedSize(1200,600);
    for(int i=0;i<3;i++){
        Menus[i]=new QTabWidget(this);
        Menus[i]->setGeometry(0,100,1200,500);
    }
    //Menus[0] rule
    {
    Menus[0]->show();
    MenuPrincipal[0]=new QPushButton("Play the 2 and 3",Menus[0]);
    MenuPrincipal[1]=new QPushButton("play the 4",Menus[0]);
    MenuPrincipal[2]=new QPushButton("Quit",Menus[0]);
    for(int i=0;i<3;i++){
        MenuPrincipal[i]->setGeometry(500,200+i*100,200,50);
        MenuPrincipal[i]->show();
    }
    Asks[0]=new QLabel("Which rule would you like to play?",Menus[0]);
    Asks[0]->move(358,0);
    }
    //Menus[1] gameEnd
    {Menus[1]->hide();
    Asks[1]=new QLabel("Which kind of game end would you like?",Menus[1]);
    Asks[1]->move(310,0);
    GameEnd[0]=new QPushButton("give seeds to last picker",Menus[1]);
    GameEnd[1]=new QPushButton("Give seeds to both",Menus[1]);
    GameEnd[2]=new QPushButton("Let seeds in game",Menus[1]);
    GameEnd[3]=new QPushButton("Back",Menus[1]);
    for(int i=0;i<4;i++){
        GameEnd[i]->setGeometry(500,((i+2)*75),200,50);
        GameEnd[i]->show();
    }
    //GameEnd[3]->move(0,25);
    }
    //Menus[2] variant
    {Menus[2]->hide();
    Asks[2]=new QLabel("Which rule would you like for more than 12 seeds?",Menus[2]);
    Asks[2]->move(230,0);
    Variant[0]=new QPushButton("Let a hole where taken",Menus[2]);
    Variant[1]=new QPushButton("Do not let a hole",Menus[2]);
    Variant[2]=new QPushButton("Back",Menus[2]);
    for(int i=0;i<3;i++){
        Variant[i]->setGeometry(500,200+i*100,200,50);
        Variant[i]->show();
    }
    }
    for(int i=0;i<3;i++){
        Asks[i]->setFont(QFont("Times New Roman",25,75));
        Asks[i]->show();
    }
    //board
    {
    board=new Board(this);board->hide();
    }

    //connect
    {//Menus
     {
    connect(MenuPrincipal[0], SIGNAL(clicked()), this, SLOT(setRule1()) );
    connect(MenuPrincipal[1], SIGNAL(clicked()), this, SLOT(setRule2()) );
    connect(MenuPrincipal[2], SIGNAL(clicked()), qApp, SLOT(quit()) );
    connect(GameEnd[0],SIGNAL(clicked()),this,SLOT(setGameEnd0()) );
    connect(GameEnd[1],SIGNAL(clicked()),this,SLOT(setGameEnd1()) );
    connect(GameEnd[2],SIGNAL(clicked()),this,SLOT(setGameEnd2()) );
    connect(GameEnd[3],SIGNAL(clicked()),this,SLOT(backg()) );
    connect(Variant[0],SIGNAL(clicked()),this,SLOT(setVariant0()) );
    connect(Variant[1],SIGNAL(clicked()),this,SLOT(setVariant1()) );
    connect(Variant[2],SIGNAL(clicked()),this,SLOT(backv()) );}
    //board
    connect(board,SIGNAL(changeRule()),this,SLOT(chooseRule()));
    }
}
