#include <board.h>

Board::Board(QWidget*parent) : QTabWidget(parent)
{
    this->setGeometry(100,200,1000,400);
    //places
    {int x=650,y=210;
    for(int i=0;i<12;i++){
        places[i]=new Place(this,i);
        places[i]->setGeometry(x,y,70,70);
        if(i<5) x-=85;else if(i==5) y-=90;else x+=85;
        places[i]->setNbSeeds(4);
        playable[i]=1;
    }
    }
    //player labels & taken seeds
    {
    for(int i=1;i<2;i++){
        takenSeeds[i]=new QLCDNumber(2,this);
        takenSeeds[i]->setGeometry(600,50+(!i*250),50,50);
        takenSeeds[i]->setOctMode();
        takenSeeds[i]->setSegmentStyle(QLCDNumber::Flat);
        takenSeeds[i]->show();
        Player[i]=new QLabel("player "+QString::number(i),this);
        Player[i]->setFont(QFont("Times New Roman",25,75));
        Player[i]->move(400,50+(!i*250));
        Player[i]->show();
    }
    }
    //bouton pause
    {
    Pause=new QPushButton("Pause",this);
    Pause->setGeometry(20,200,30,30);
    PauseM=new QWidget();
    PauseM->setFixedSize(150,200);
    MPause[0]=new QPushButton("resume",PauseM);
    MPause[1]=new QPushButton("Quit Game",PauseM);
    MPause[2]=new QPushButton("Quit All",PauseM);
    for(int i=0;i<3;i++){
        MPause[i]->setGeometry(0,75*i,150,50);
        MPause[i]->show();
    }
    }
    //Winner
    {
    Winner=new QWidget();
    Winner->setFixedSize(200,130);
    xWon[0]=new QLabel("DRAW",Winner);xWon[0]->hide();
    xWon[1]=new QLabel("Player 1 won\nthe game",Winner);xWon[1]->hide();
    xWon[2]=new QLabel("Player 2 won\nthe game",Winner);xWon[2]->hide();
    xWon[1]->setGeometry(50,0,100,50);
    xWon[2]->setGeometry(50,0,100,50);
    WOptions[0]=new QPushButton("  Play again  ",Winner);
    WOptions[1]=new QPushButton(" Change rules ",Winner);
    WOptions[2]=new QPushButton("  Quit  ",Winner);
    WOptions[0]->move(0,60);
    WOptions[1]->move(100,60);
    WOptions[2]->move(50,100);
    for(int i=0;i<3;i++){
        xWon[0]->setGeometry(50+(!i*10),0,100,50);
        WOptions[i]->show();
    }
    }
    //connect
    {
    //places
    for(int i=0;i<12;i++)
        connect(places[i],SIGNAL(playHere(int)),this,SLOT(doATurn(int)));
    //Pause
    {
    connect(Pause,SIGNAL(clicked()),this,SLOT(pause()) );
    connect(MPause[0],SIGNAL(clicked()),this,SLOT(resume()) );
    connect(MPause[1],SIGNAL(clicked()),this,SLOT(quitGame()) );
    connect(MPause[2],SIGNAL(clicked()),qApp,SLOT(quit()));
    }
    //Winner
    {
    connect(this,SIGNAL(winner(int)),this,SLOT(winning(int)));
    connect(WOptions[0],SIGNAL(clicked()),this,SLOT(regame()) );
    connect(WOptions[1],SIGNAL(clicked()),this,SLOT(quitGame()) );
    connect(WOptions[2],SIGNAL(clicked()),qApp,SLOT(quit()) );
    }
}
}

void Board::setRule(int r)
{
    rule=r;
}
void Board::setGameEnd(int g)
{
    gameEnd=g;
}
void Board::setVariant(int v)
{
    variant=v;
}
void Board::initialize()
{
    player=nbGames%2;
    for(int i=0;i<12;i++)
    {
        playable[i]=true;
        places[i]->setNbSeeds(4);
        places[i]->setCheckable(true);
    }
    takenSeeds[0]->display(0);
    takenSeeds[1]->display(0);
}

void Board::pause()
{
    PauseM->show();
    for(int i=0;i<12;i++)
            places[i]->setCheckable(false);
    Pause->setCheckable(false);
}
void Board::resume()
{
    PauseM->hide();
    for(int i=0;i<12;i++)
            places[i]->setCheckable(true);
    Pause->setCheckable(true);
}
void Board::regame()
{
    Winner->hide();
    this->initialize();
    Pause->setCheckable(true);
    nbGames++;
}
void Board::quitGame()
{
    PauseM->hide();
    this->hide();
    this->initialize();
    emit changeRule();
}

void Board::winning(int w)
{   for(int i=0;i<12;i++)
        places[i]->setCheckable(false);
    Pause->setCheckable(false);
    Winner->show();
    xWon[w]->show();
}

int sommeTab(Place* t[])//tells the total of seeds on the board
{   int s(0);
    for(int i=0;i<12;i++){
        s+=t[i]->getNbSeeds();}
    return s;
}
int sommedTab(Place* t[], int d)//tells the total of seeds from d to d+5
{int s(0);
    for(int i=d;i<d+6;i++){
        s+=t[i]->getNbSeeds();}
    return s;
}
bool tsFaux(bool t[], int d)//tests holes from d to d+5
{bool s(false);
    for(int i=d;!s&&i<d+6;i++)
        s=s||t[i];
    return !s;
}
void Board::doATurn(int dealPos)
{
    int w=-1;
   // w->*((Board*)this)->Board::turn[rule](dealPos);
    /*switch(rule)
    {
        case 0 :*/ w=turn23(dealPos);
     /*
        case 1 : w=turn4(dealPos);
    }*/
    if(w!=-1) emit winner(w);
}
int Board::turn23(int dealPos)
{
    int hand,giving,a=player,b=(!player);
    //setting playable
    {
    for(int i=0;i<12;i++){
        if(places[i]->getNbSeeds()==0)playable[i]=0;
        else playable[i]=1;
    }
    int d=sommedTab(places,b);
    switch(d){
         case 0:{
        for(int i=a*6;i<(a+1)*6;i++)
            if(i-places[i]->getNbSeeds()>(a*6)-1)
                playable[i]=0;
        break;}
         case 1:{
        if(places[11]->getNbSeeds()/*==1*/)
              for(int i=a*6;i<(a+1)*6;i++)
                  if(i-places[i]->getNbSeeds()==(a*6)-1)
                      playable[i]=0;
        break;}
         case 2:{
        if(places[11]->getNbSeeds()==2)
            for(int i=a*6;i<(a+1)*6;i++)
                if(i-places[i]->getNbSeeds()==(a*6)-1)
                    playable[i]=0;
        break;}
    }
    if(sommedTab(places,b*6)==0 && tsFaux(playable,a*6))return !player;
    if(!playable[dealPos])return -1;
    }

    hand=places[dealPos]->getNbSeeds();
    places[dealPos]->setNbSeeds(0);
    giving=dealPos;
    while(hand>0){
       giving--; if (giving<0)giving=11;
       places[giving]->addSeed(); hand--;}

    int bUp=b-1,bDown=b+6; bool taking(true);
    while(giving>bDown && giving<bUp && taking){
        int number=places[giving]->getNbSeeds();
        if (number==2 || number==3){
          takenSeeds[player]->display(takenSeeds[player]->intValue()+number);
          places[giving]->setNbSeeds(0);}
        else taking=false;
        giving++;
        }
    int totalSeeds=sommeTab(places),
            diffSeeds=int(takenSeeds[1]->value()-takenSeeds[0]->value());
    if(totalSeeds>=4) {player=!player;return -1;}
    switch(gameEnd){
       case 1:{if(diffSeeds<=totalSeeds && (-diffSeeds)<=totalSeeds)return 0;break;}
       case 0:{takenSeeds[player]->display(takenSeeds[player]->intValue()+totalSeeds);break;}
       }
    diffSeeds=takenSeeds[1]->intValue()-takenSeeds[0]->intValue();
    if(diffSeeds<0)return 1;
    else if(diffSeeds>0)return 2;
    else return 0;
}

/*int Board::turn4(int dealPos)
{return -1;}
*/
