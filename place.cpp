#include <place.h>

Place::Place(QWidget *parent,int p,int s) : QPushButton(parent)
{
    //show();
    seeds=new QLCDNumber(2,this);
    //seeds->show();
    position=p;
    seeds->display(s);
    seeds->setOctMode();
    seeds->setGeometry(0,0,70,35);
    seeds->setSegmentStyle(QLCDNumber::Flat);
    connect(this,SIGNAL(clicked()),this,SLOT(click()));
}

int Place::getPos()
{
    return position;
}
void Place::setPos(int p)
{
    position=p;
}

int Place::getNbSeeds()
{
    return seeds->intValue();
}
void Place::setNbSeeds(int n)
{
    seeds->display(n);
}
void Place::addSeed(int n)
{
    seeds->display(seeds->intValue()+n);
}

void Place::click()
{
    emit playHere(position);
}
