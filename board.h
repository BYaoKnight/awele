#ifndef BOARD_H
#define BOARD_H

#include "place.h"
#include <QApplication>
#include <QTabWidget>
#include <QLabel>

class Board : public QTabWidget
{
    Q_OBJECT
public :
    Board(QWidget*parent);
    void setRule(int r);
    void setVariant(int v);
    void setGameEnd(int g);
    void initialize();

    int turn23(int dealPos);
    //int turn4(int dealPos);

public slots :
    void doATurn(int dealPos);

    void pause();
    void winning(int w);
    void resume();
    void regame();
    void quitGame();
signals :
    void changeRule();
    void winner(int w);

private :
    QPushButton* Pause;
    QWidget* PauseM;
    QPushButton* MPause[3];
    QLabel* Player[2];

    QWidget* Winner;
    QPushButton* WOptions[3];
    QLabel* xWon[3];

    int rule=0,variant=0,gameEnd=0,nbGames=0;
    Place* places[12];
    bool player=0,playable[12]={0};
    QLCDNumber* takenSeeds[2];
    //int (Board::*turn[2])(int dealPos)={&(turn23),&(turn4)};

};

#endif // BOARD_H
