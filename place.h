#ifndef PLACE_H
#define PLACE_H

#include <QPushButton>
#include <QLCDNumber>

class Place : public QPushButton
{
    Q_OBJECT
public :
    Place(QWidget*parent,int p,int s=4);
    int getPos();
    void setPos(int p);
    int getNbSeeds();
    void setNbSeeds(int n);
    void addSeed(int n=1);
public slots :
    void click();
signals :
    void playHere(int pos);
private :
    QLCDNumber* seeds;
    int position;
};

#endif // PLACE_H
