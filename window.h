#ifndef WINDOW_H
#define WINDOW_H

#include "board.h"

class Window : public QWidget
{
  Q_OBJECT
public :
  Window();
  void setRule(int r);
  void setVariant(int v);
  void setGameEnd(int g);

public slots :
  void setRule1();
  void setRule2();
  void setGameEnd0();
  void setGameEnd1();
  void setGameEnd2();
  void backg();
  void setVariant0();
  void setVariant1();
  void backv();
  void chooseRule();

private :
  QTabWidget* Menus[3];
  Board* board;
  QPushButton* MenuPrincipal[3];
  QPushButton* GameEnd[4];
  QPushButton* Variant[3];
  QLabel* Asks[3];

};
#endif // WINDOW_H
